#!/bin/bash
echo "First arg: $1"
echo "Second arg: $2"
echo "Third arg: $3"
if [ $1 == "GET" ]
then
	if [ $2 == "client" ]
	then
	curl -v "http://localhost:8080/clients/$3"
	fi
fi
if [ $1 == "POST" ]
then
	if [ $2 == "deposit" ]
	then
	curl -d "$3" -H "Content-Type: application/json" "http://localhost:8080/clients/deposit"
	fi
	if [ $2 == "withdrawal" ]
	then
	curl -d "$3" -H "Content-Type: application/json" "http://localhost:8080/clients/withdrawal"
	fi
fi
