package com.veritran.bankapp.service;

import com.veritran.bankapp.dto.ClientDTO;
import com.veritran.bankapp.exception.NegativeSalary;
import com.veritran.bankapp.repository.ClientRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ClientServiceTest {

    @Mock
    private ClientRepository repository;

    @Autowired
    @InjectMocks
    private ClientServiceImpl service;

    @Test
    public void shouldIncreaseBalanceWhenDepositIsMade() {
        //given
        ClientDTO clientBeforeDeposit = service.findByAccountId("francisco");
        assertEquals(Double.parseDouble("0.00"), clientBeforeDeposit.getMoney());
        //when
        service.depositMoney("francisco", Double.parseDouble("100"));
        //then
        ClientDTO clientAfterDeposit = service.findByAccountId("francisco");
        assertEquals(Double.parseDouble("100"), clientAfterDeposit.getMoney());
    }

    @Test
    public void shouldThrowAnErrorWhenDepositSalaryIsNegative() {
        //given
        Double negativeSalary = Double.parseDouble("-100");
        //when
        //then
        assertThrows(NegativeSalary.class,
                () -> {
                    service.depositMoney("francisco", negativeSalary);
                });
    }
}
