DROP TABLE IF EXISTS clients;

CREATE TABLE clients (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  account_id VARCHAR(250) NOT NULL,
  money DECIMAL DEFAULT NULL
);

INSERT INTO clients (name, account_id, money) VALUES
  ('Fran', 'francisco', '0.00'),
  ('Mauro', '1234', '0.00'),
  ('Juan', '12345', '0.00');