package com.veritran.bankapp.dto;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity(name = "clients")
public class ClientDTO {
    @Id
    private Long id;
    @Column
    private String name;
    @Column(name = "account_id")
    private String accountId;
    @Column(name = "money")
    private Double money;
}
