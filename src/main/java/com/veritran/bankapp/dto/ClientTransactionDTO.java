package com.veritran.bankapp.dto;

import lombok.Data;

@Data
public class ClientTransactionDTO {
    private String accountId;
    private Double money;
}
