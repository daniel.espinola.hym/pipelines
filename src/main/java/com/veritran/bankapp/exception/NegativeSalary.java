package com.veritran.bankapp.exception;

public class NegativeSalary extends RuntimeException {
    public NegativeSalary(String message) {
        super(message);
    }
}
