package com.veritran.bankapp.controller;

import com.veritran.bankapp.dto.ClientDTO;
import com.veritran.bankapp.dto.ClientTransactionDTO;
import com.veritran.bankapp.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/clients")
public class ClientController {

    private final ClientService service;

    @Autowired
    public ClientController(ClientService service) {
        this.service = service;
    }

    @GetMapping("/{accountId}")
    public ClientDTO getClientById(@PathVariable String accountId) {
        return service.findByAccountId(accountId);
    }

    @PostMapping("/deposit")
    public ClientDTO depositMoney(@RequestBody ClientTransactionDTO dto) {
        return service.depositMoney(dto.getAccountId(), dto.getMoney());
    }
}
