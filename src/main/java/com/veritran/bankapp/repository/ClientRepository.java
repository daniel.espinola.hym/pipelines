package com.veritran.bankapp.repository;

import com.veritran.bankapp.dto.ClientDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<ClientDTO, Long> {
    ClientDTO findByAccountId(String accountId);
}
