package com.veritran.bankapp.service;

import com.veritran.bankapp.dto.ClientDTO;
import com.veritran.bankapp.exception.NegativeSalary;
import com.veritran.bankapp.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements ClientService {
    private final ClientRepository repository;

    @Autowired
    public ClientServiceImpl(ClientRepository repository) {
        this.repository = repository;
    }

    @Override
    public ClientDTO findByAccountId(String accountId) {
        return this.repository.findByAccountId(accountId);
    }

    @Override
    public ClientDTO depositMoney(String accountId, Double money) {
        this.validateMoney(money);
        ClientDTO client = this.repository.findByAccountId(accountId);
        client.setMoney(client.getMoney() + money);
        return this.repository.save(client);
    }

    private void validateMoney(Double money) {
        if(money < 0) {
            throw new NegativeSalary("You cannot deposit a negative salary");
        }
    }
}
