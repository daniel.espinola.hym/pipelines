package com.veritran.bankapp.service;

import com.veritran.bankapp.dto.ClientDTO;

public interface ClientService {
    ClientDTO findByAccountId(String accountId);
    ClientDTO depositMoney(String accountId, Double money);
}
